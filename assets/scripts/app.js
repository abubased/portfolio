const Portfolio = function() {
	function makeWords() {
		var words = [
			{
				text: "React JS",
				weight: 12.3
			}, {
				text: "css3",
				weight: 8
			}, {
				text: "javascript",
				weight: 14
			}, {
				text: "HTML5",
				weight: 3
			}, {
				text: "Bootstrap",
				weight: 7
			}, {
				text: "PHP",
				weight: 10
			}, {
				text: "jQuery",
				weight: 9
			}, {
				text: "Laravel",
				weight: 15
			}, {
				text: "Wordpress",
				weight: 7
			}
		];
		return words;
	}

	function makeWordCloud(words) {
		$('.teaching-domains').jQCloud(words, {delay: 120});
	}

	function displayWordCloud() {
		var count = 1;
		$(window).on('scroll', function() {
			var y_scroll_pos = window.pageYOffset;
			var scroll_pos_test = 2700; // set to whatever you want it to be
			var words = makeWords();
			if (y_scroll_pos > scroll_pos_test && count <= 1) {
				makeWordCloud(words);
				count++;
			}
		});
	}

	function designForm() {
		$("#my-modal form").addClass("my-form");
	}

	function typeAnimation() {
		Typed.new("#writing-text", {
			strings: [
				"am a Full-Stack Web Developer.", "I always want to deliver my best within","website designing and development area","because programming is my passion.", "also teach programming to peoples.","solve problems.","<a href='../assets/files/abuBasedCv.pdf' target='_blank' class='btn btn-fill wow fadeInUp' data-wow-duration='0.8s' data-wow-delay='0.4s' style='visibility: visible; animation-duration: 0.8s; animation-delay: 0.4s; animation-name: fadeInUp;'>Download my CV <i class='fa fa-download'></i></a>"
			],
			
			// Optionally use an HTML element to grab strings from (must wrap each string in a <p>)
			// typing speed
			typeSpeed: 10,
			backSpeed: -100,
			// contentType: 'text',
			callback: function() {
				$("#writing-text")
				$("#writing-text").css({"color": "#fff", "background-color":"rgba(58, 136, 194,.5)"});
			},
			preStringTyped: function() {},
			onStringTyped: function() {},
			
		});
	}

	return {
		displayWordCloud: displayWordCloud,
		typeAnimation: typeAnimation
	}

}();


Portfolio.displayWordCloud();
Portfolio.typeAnimation();
